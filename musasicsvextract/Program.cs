﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net.Http;
using MySql.Data.MySqlClient;
using LumenWorks.Framework.IO.Csv;

namespace musasicsvextract
{
    class Program
    {
        static string c_date = DateTime.Now.ToString("yyyy-MM-dd");
        private static readonly HttpClient client = new HttpClient();
        public static string oDate;
        public static string MachName;
        public static string ProgName;
        public static string oProcstarttime;
        public static string oProcendtime;
        public static TimeSpan ProcTime;
        public static string oMachOprTime;
        public static string oLaserProcTime;
        public static string oNCTProcTime;
        public static TimeSpan NCTProcTime;
        public static string oAlarmTime;
        public static TimeSpan AlarmTime;
        public static string MatName;
        public static string oThickness;
        public static string oSheetX;
        public static string oSheetY;
        public static string oYieldRate;
        public static string oGood;
        public static string oNGQty;
        public static string Speed;
        static void Main(string[] args)
        {
            string connstring = "Server = localhost; Database = factory; Uid = root; Pwd = ;SslMode=none";
            MySqlConnection mcon = new MySqlConnection(connstring);
            mcon.Open();
            string connstring_setupdb = "Server = localhost; Database = setupsheetdb; Uid = root; Pwd = ;SslMode=none";
            MySqlConnection mcon_setup = new MySqlConnection(connstring_setupdb);
            mcon_setup.Open();
            try
            {
                string cur_date = DateTime.Now.ToString("yyyy-MM");
                string CmdText1 = "DELETE FROM musasicsvquickdata WHERE procend NOT like '" + cur_date + "%' ";
                using (MySqlCommand cmd1 = new MySqlCommand(CmdText1, mcon))
                {
                cmd1.ExecuteNonQuery();
                }
                CmdText1 = "DELETE FROM musasicsvextract WHERE date like '" + cur_date + "%' ";
                using (MySqlCommand cmd1 = new MySqlCommand(CmdText1, mcon))
                {

                    cmd1.ExecuteNonQuery();
                }

                var files = Directory.GetFiles(@"F:\xampp\htdocs\alfadockpro\factory_layout\473\RPA", "*.csv");

                foreach (String f in files)
                {
                    string fname = System.IO.Path.GetFileNameWithoutExtension(f);
                    if (f.Contains("Laser-ProcessingData"))
                    {
                        extract_eml_processdata(f, mcon, mcon_setup);
                    }
                    else if (f.Contains("Bending-ProcessingData"))
                    {
                        extract_bend_processdata(f, mcon);
                    }
                    else if (f.Contains("EML-ProcessingData"))
                    {
                        extract_eml_processdata(f, mcon, mcon_setup);
                    }
                    else if (f.Contains("quick_view_data"))
                    {
                        extract_quickview_info(f, mcon, mcon_setup);
                    }
                    else if (f.Contains("OEEData"))
                    {
                        extract_oeee_info(f, mcon);
                    }
                    else if (f.Contains("DailyProductInfo"))
                    {
                        extract_product_info(f, mcon);
                    }
                    else if (f.Contains("CumulativeProduction"))
                    {
                        extract_cum_product_info(f, mcon);
                    }
                }
            }
            catch (Exception e)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("" + e, w);
                }
            }
            finally
            {
                if (mcon != null)
                {
                    mcon.Close();
                }
                if (mcon_setup != null)
                {
                    mcon_setup.Close();
                }
            }
        }
        public static void extract_quickview_info(string path, MySqlConnection mcon, MySqlConnection mcon2)
        {
            string val_id = "";
            if (path.Contains("ENSIS"))
            {
                val_id = "en";

            }
            else if (path.Contains("ACIES"))
            {
                val_id = "ac";
            }
            else if (path.Contains("VN3"))
            {
                val_id = "vn";
            }
            else if (path.Contains("FBD III"))
            {
                val_id = "fbd";
            }
            else if (path.Contains("HD-1303NT13034052"))
            {
                val_id = "1hd";
            }
            else if (path.Contains("HD-1303NT13034281"))
            {
                val_id = "2hd";
            }
            else if (path.Contains("HDS"))
            {
                val_id = "hds";
            }

            string fileName;
            string Act_fileName;
            string MachName;
            string ProgNo;
            string procstrt;
            string procend;
            string goodqty;
            string defqty;
            TimeSpan ProcTime;
            try
            {
                using (CsvReader csv = new CsvReader(new StreamReader(path), true))
                {
                    int counter = 1;
                    fileName = Path.GetFileNameWithoutExtension(path);
                    Act_fileName = fileName.Replace("-quick_view_data", "");
                    if (Act_fileName.StartsWith("ACIES"))
                    {
                        Act_fileName = "ACIES";
                    }
                    else if (Act_fileName.StartsWith("VN3015AJ"))
                    {
                        Act_fileName = "VN3015AJ";
                    }
                    else if (Act_fileName.StartsWith("ENSIS-3015AJ"))
                    {
                        Act_fileName = "ENSIS-3015AJ";
                    }
                    else if (Act_fileName.StartsWith("FBD"))
                    {
                        Act_fileName = "FBD";
                    }

                    int fieldCount = csv.FieldCount;
                    string[] headers = csv.GetFieldHeaders();
                    foreach (string headval in headers)
                    {
                        if (headval.Contains("アラームメッセージ"))
                            return;
                    }
                    while (csv.ReadNextRecord())
                    {


                        //Machine Name
                        MachName = Act_fileName;

                        //Program Name
                        ProgNo = csv[1];

                        DateTime Date1 = new DateTime();
                        DateTime Date2 = new DateTime();
                        if (Act_fileName.Contains("ENSIS") || Act_fileName.Contains("ACIES") || Act_fileName.Contains("VN"))
                        {
                            //Process start time
                            procstrt = csv[4];
                            Date1 = DateTime.Parse(procstrt);

                            //Process end time
                            procend = csv[5];
                            Date2 = DateTime.Parse(procend);

                            //Good Qty
                            goodqty = csv[6];

                            //Defective Qty
                            defqty = csv[7];
                            //Process Time
                            ProcTime = Date2 - Date1;

                            int def_qty = Convert.ToInt16(defqty);
                           
                           
                        }
                        else
                        {
                            //Process start time
                            procstrt = csv[3];
                            Date1 = DateTime.Parse(procstrt);

                            //Process end time
                            procend = csv[4];
                            Date2 = DateTime.Parse(procend);

                            //Good Qty
                            goodqty = csv[5];

                            //Defective Qty
                            defqty = csv[6];
                            //Process Time
                            ProcTime = Date2 - Date1;
                        }
                        double ProcTimeSecs = ProcTime.TotalSeconds;

                        string val_proc = procstrt.Replace("/", "");
                        val_proc = procstrt.Replace(":", "");
                        string CmdText = "INSERT IGNORE INTO musasicsvquickdata VALUES(@idcsv,@machname,@progno,@procstrt,@procend,@goodqty,@defqty,@proctimesecs)";
                        MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                        cmd.Parameters.AddWithValue("@idcsv", val_id + "-" + val_proc);
                        cmd.Parameters.AddWithValue("@machname", MachName);
                        cmd.Parameters.AddWithValue("@progno", ProgNo);
                        cmd.Parameters.AddWithValue("@procstrt", Date1);
                        cmd.Parameters.AddWithValue("@procend", Date2);
                        cmd.Parameters.AddWithValue("@goodqty", goodqty);
                        cmd.Parameters.AddWithValue("@defqty", defqty);
                        cmd.Parameters.AddWithValue("@proctimesecs", ProcTimeSecs);

                       int res= cmd.ExecuteNonQuery();
                        counter++;
                        int good_qty = Convert.ToInt32(goodqty);
                        if (res == 1 && good_qty > 0)
                        {
                            Program pg = new Program();
                            if (MachName.StartsWith("ACIES"))
                            {
                                pg.update_GPN(ProgNo, "12273", goodqty, "1588", false, "-1", procstrt.Replace("/", "-"), procend.Replace("/", "-"), c_date);
                            }
                            else if (MachName.StartsWith("EN") || MachName.StartsWith("VN"))
                            {
                                pg.update_GPN(ProgNo, "12418", goodqty, "1588", false, "-1", procstrt.Replace("/", "-"), procend.Replace("/", "-"), c_date);

                            }
                            else
                            {
                                pg.update_GPN(ProgNo, "12419", goodqty, "1588", false, "-1", procstrt.Replace("/", "-"), procend.Replace("/", "-"), c_date);

                            }
                            pg = null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log(""+e, w);
                }
            }
        }
        async void update_GPN(String partno, string pid, string qt, string userid, bool str_datefilter, string orderno, string start_date, string end_date, string due_date)
        {
            var str_arg = "{\"partNo\":\"" + partno + "\",\"processid\":\"" + pid + "\",\"status\":\"2\",\"quantity\":\"" + qt + "\",\"userid\":\"" + userid + "\",\"isSearchApplyDateFilters\":\"false\",\"setGivenTime\":\"true\",\"orderNo\":\"" + orderno + "\",\"startdate\":\"" + start_date + "\",\"enddate\":\"" + end_date + "\",\"dueDate\":\"" + due_date + "\"}";
           using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log(str_arg, w);
            }
            var values = new Dictionary<string, string>
{
    { "plugin", "SchedulerAPI" },
    { "controller", "SchedulerSubmitProcessController" },
    { "action", "SiProcessForLoadBalance" },
    { "args", str_arg }
};

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("http://13.115.237.181/api/plugin", content);
            
            await response.Content.ReadAsStringAsync();

        }
        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            w.WriteLine("  :");
            w.WriteLine($"  :{logMessage}");
            w.WriteLine("-------------------------------");
        }
        public static void extract_oeee_info(string path, MySqlConnection mcon)
        {
            string val_id = "";
            if (path.Contains("Laser"))
            {
                val_id = "ls";

            }
            else if (path.Contains("Bending"))
            {
                val_id = "ben";
            }
            else if (path.Contains("EML"))
            {
                val_id = "em";
            }

            string MachName;

            string date;
            float optime;
            float settime;
            float idletime;
            float errtime;
            using (CsvReader csv =
             new CsvReader(new StreamReader(path), true))
            {
                int counter = 1;
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                while (csv.ReadNextRecord())
                {
                    //for (int i = 0; i < fieldCount; i++)
                    //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));


                    //Machine Name
                    MachName = csv[0];
                    if (MachName.StartsWith("ACIES"))
                    {
                        MachName = "ACIES";
                    }
                    else if (MachName.StartsWith("VN3015AJ"))
                    {
                        MachName = "VN3015AJ";
                    }
                    else if (MachName.StartsWith("ENSIS-3015AJ"))
                    {
                        MachName = "ENSIS-3015AJ";
                    }
                    else if (MachName.StartsWith("FBD"))
                    {
                        MachName = "FBD";
                    }
                    else if (MachName.Contains("13034052"))
                    {
                        MachName = "HD-1303NT13034052";
                    }
                    else if (MachName.Contains("13034281"))
                    {
                        MachName = "HD-1303NT13034281";
                    }
                    else if (MachName.Contains("82504305"))
                    {
                        MachName = "HDS-8025NT80504305";
                    }
                    //Date
                    date = csv[1];


                    //time
                    optime = float.Parse(csv[2]);
                    settime = float.Parse(csv[3]);
                    idletime = float.Parse(csv[4]);
                    errtime = float.Parse(csv[5]);

                    float Totaltime = optime + settime + idletime + errtime;
                    float op_percent = optime;
                    float set_percent = settime;
                    float idle_percent = idletime;
                    float err_percent = errtime;
                    /*  if (Totaltime != 0)
                      {

                           op_percent = (optime * 100) / Totaltime;
                          set_percent = (settime * 100) / Totaltime;
                           idle_percent = (idletime * 100) / Totaltime;
                           err_percent = (errtime * 100) / Totaltime;
                      }*/

                    string CmdText = "INSERT IGNORE INTO musasicsvtime VALUES(@idcsv,@machname,@date,@optime,@settime,@idletime,@errtime,@tottime,@avgvalue)";
                    MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                    cmd.Parameters.AddWithValue("@idcsv", date + "_" + val_id + "_" + counter);
                    cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                    cmd.Parameters.AddWithValue("@date", date);
                    cmd.Parameters.AddWithValue("@optime", op_percent);
                    cmd.Parameters.AddWithValue("@settime", set_percent);
                    cmd.Parameters.AddWithValue("@idletime", idle_percent);
                    cmd.Parameters.AddWithValue("@errtime", err_percent);
                    cmd.Parameters.AddWithValue("@tottime", Totaltime);
                    cmd.Parameters.AddWithValue("@avgvalue", 100);


                    int result = cmd.ExecuteNonQuery();
                    if (result != 1)
                    {
                        string query = string.Format(@"UPDATE musasicsvtime SET optime={1} , settime={2}, idletime={3}, errtime={4}, tottime={5} where NOT tottime ={5} AND idcsv='{0}'", date + "_" + val_id + "_" + counter, op_percent, set_percent, idle_percent, err_percent, Totaltime);


                        using (MySqlCommand command = new MySqlCommand(query, mcon))
                        {
                            int row = command.ExecuteNonQuery();
                        }
                    }
                    counter++;

                }
            }
        }

        public static void extract_cum_product_info(string path, MySqlConnection mcon)
        {
            string val_id = "";
            if (path.Contains("Laser"))
            {
                val_id = "ls";

            }
            else if (path.Contains("Bending"))
            {
                val_id = "b";
            }
            else if (path.Contains("EML"))
            {
                val_id = "em";
            }
            string MachName;
            string Str_ActProcCount;
            string Str_TargetCount;
            using (CsvReader csv = new CsvReader(new StreamReader(path), true))
            {
                int counter = 1;
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                while (csv.ReadNextRecord())
                {
                    //for (int i = 0; i < fieldCount; i++)
                    //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));


                    //Machine Name
                    MachName = csv[0];
                    if (MachName.StartsWith("ACIES"))
                    {
                        MachName = "ACIES";
                    }
                    else if (MachName.StartsWith("VN3015AJ"))
                    {
                        MachName = "VN3015AJ";
                    }
                    else if (MachName.StartsWith("ENSIS-3015AJ"))
                    {
                        MachName = "ENSIS-3015AJ";
                    }
                    else if (MachName.StartsWith("FBD"))
                    {
                        MachName = "FBD";
                    }
                    else if (MachName.Contains("13034052"))
                    {
                        MachName = "HD-1303NT13034052";
                    }
                    else if (MachName.Contains("13034281"))
                    {
                        MachName = "HD-1303NT13034281";
                    }
                    else if (MachName.Contains("82504305"))
                    {
                        MachName = "HDS-8025NT80504305";
                    }
                    //Actual Process Count
                    Str_ActProcCount = csv[1];

                    //Target Process Count
                    Str_TargetCount = csv[2];



                    string CmdText = "INSERT IGNORE INTO musasicsvcumprocess VALUES(@idcsv,@machname,@actproccount,@targetcount)";
                    MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                    cmd.Parameters.AddWithValue("@idcsv", "2020-05-31" + "_" + val_id + "_" + counter);
                    cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                    cmd.Parameters.AddWithValue("@actproccount", Str_ActProcCount);
                    cmd.Parameters.AddWithValue("@targetcount", Str_TargetCount);


                    int result = cmd.ExecuteNonQuery();
                    if (result != 1)
                    {
                        string query = string.Format(@"UPDATE musasicsvcumprocess SET actproccount={1}, targetcount={2} where (NOT actproccount ={1}) AND idcsv='{0}'", "2020-05-31" + "_" + val_id + "_" + counter, Str_ActProcCount, Str_TargetCount);


                        using (MySqlCommand command = new MySqlCommand(query, mcon))
                        {
                            int row = command.ExecuteNonQuery();
                        }
                    }
                    counter++;
                }

            }
        }

        public static void extract_product_info(string path, MySqlConnection mcon)
        {
            string val_id = "";
            if (path.Contains("Laser"))
            {
                val_id = "ls";

            }
            else if (path.Contains("Bending"))
            {
                val_id = "ben";
            }
            else if (path.Contains("EML"))
            {
                val_id = "em";
            }
            string MachName;
            string str_Date;
            string Str_ActProcCount;
            string Str_TargetCount;
            using (CsvReader csv = new CsvReader(new StreamReader(path), true))
            {
                int counter = 1;
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                while (csv.ReadNextRecord())
                {
                    //for (int i = 0; i < fieldCount; i++)
                    //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));


                    //Machine Name
                    MachName = csv[0];
                    if (MachName.StartsWith("ACIES"))
                    {
                        MachName = "ACIES";
                    }else if (MachName.StartsWith("VN3015AJ"))
                    {
                        MachName = "VN3015AJ";
                    }
                    else if (MachName.StartsWith("ENSIS-3015AJ"))
                    {
                        MachName = "ENSIS-3015AJ";
                    }
                    else if (MachName.StartsWith("FBD"))
                    {
                        MachName = "FBD";
                    }
                    else if (MachName.Contains("13034052"))
                    {
                        MachName = "HD-1303NT13034052";
                    }
                    else if (MachName.Contains("13034281"))
                    {
                        MachName = "HD-1303NT13034281";
                    }
                    else if (MachName.Contains("82504305"))
                    {
                        MachName = "HDS-8025NT80504305";
                    }
                    //Actual Process Count
                    Str_ActProcCount = csv[4];

                    //Target Process Count
                    Str_TargetCount = csv[5];

                    //Date
                    str_Date = csv[1] + "/" + csv[2] + "/" + csv[3];
                    DateTime Date = DateTime.Parse(str_Date);

                    string str_Date_1 = csv[1] + "-" + csv[2] + "-" + csv[3];

                    string CmdText = "INSERT IGNORE INTO musasicsvprocess VALUES(@idcsv,@machname,@date,@actproccount,@targetcount)";
                    MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                    cmd.Parameters.AddWithValue("@idcsv", str_Date_1 + "_" + val_id + "_" + counter);
                    cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                    cmd.Parameters.AddWithValue("@date", Date);
                    cmd.Parameters.AddWithValue("@actproccount", Str_ActProcCount);
                    cmd.Parameters.AddWithValue("@targetcount", Str_TargetCount);


                    int result = cmd.ExecuteNonQuery();
                    if (result != 1)
                    {
                        string query = string.Format(@"UPDATE musasicsvprocess SET actproccount={1}, targetcount={2} where NOT actproccount ={1} AND idcsv='{0}'", str_Date_1 + "_" + val_id + "_" + counter, Str_ActProcCount, Str_TargetCount);


                        using (MySqlCommand command = new MySqlCommand(query, mcon))
                        {
                            int row = command.ExecuteNonQuery();
                        }
                    }
                    counter++;
                }

            }
        }

        public static void extract_bend_processdata(string path, MySqlConnection mcon)
        {

            using (CsvReader csv =
                        new CsvReader(new StreamReader(path), true))
            {

                int counter = 1;
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                while (csv.ReadNextRecord())
                {
                    //for (int i = 0; i < fieldCount; i++)
                    //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));

                    // Date
                    oDate = csv[0];
                    DateTime Date = DateTime.Parse(oDate);

                    //Machine Name
                    MachName = csv[1];
                    if (MachName.StartsWith("FBD"))
                    {
                        MachName = "FBD";
                    }
                    else if (MachName.Contains("13034052"))
                    {
                        MachName = "HD-1303NT13034052";
                    }
                    else if (MachName.Contains("13034281"))
                    {
                        MachName = "HD-1303NT13034281";
                    }
                    else if (MachName.Contains("82504305"))
                    {
                        MachName = "HDS-8025NT80504305";
                    }
                    //Program Name
                    ProgName = csv[2];

                    //Process start time
                    oProcstarttime = csv[4];
                    DateTime Procstarttime = DateTime.Parse(oProcstarttime);


                    //Process end time
                    oProcendtime = csv[5];
                    DateTime Procendtime = DateTime.Parse(oProcendtime);

                    //Processing Time
                    ProcTime = Procendtime - Procstarttime;
                    double ProcTimeSecs = ProcTime.TotalSeconds;

                    //Machine Operation Time
                    oMachOprTime = csv[6];
                    TimeSpan MachOprTime = TimeSpan.Parse(oMachOprTime);
                    double OprTimeSecs = MachOprTime.TotalSeconds;


                    //Alarm Time
                    oAlarmTime = csv[7];
                    if (oAlarmTime.Contains("-"))
                    {
                        oAlarmTime = "00:00:00";
                    }
                    AlarmTime = TimeSpan.Parse(oAlarmTime);
                    double AlarmTimeSecs = AlarmTime.TotalSeconds;

                    //Material name
                    MatName = csv[8];


                    string CmdText = "INSERT IGNORE INTO musasicsvextract VALUES(@idcsv,@date,@machname,@progname,@procstrtime,@procendtime,@proctime,@machoprtime,@laserproctime,@nctproctime,@alarmtime,@matname,@thickness,@sheetx,@sheety,@yieldrate,@good,@ngqty,@speed,@proctimesecs,@alarmtimesecs,@oprtimesecs)";
                    MySqlCommand cmd = new MySqlCommand(CmdText, mcon);

                    cmd.Parameters.AddWithValue("@idcsv", oDate + "_" + "be" + counter);
                    cmd.Parameters.AddWithValue("@date", Date);
                    cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                    cmd.Parameters.AddWithValue("@progname", ProgName);
                    cmd.Parameters.AddWithValue("@procstrtime", Procstarttime);
                    cmd.Parameters.AddWithValue("@procendtime", Procendtime);
                    cmd.Parameters.AddWithValue("@proctime", ProcTime);
                    cmd.Parameters.AddWithValue("@machoprtime", MachOprTime);
                    cmd.Parameters.AddWithValue("@laserproctime", "00:00:00");
                    cmd.Parameters.AddWithValue("@nctproctime", NCTProcTime);
                    cmd.Parameters.AddWithValue("@alarmtime", AlarmTime);
                    cmd.Parameters.AddWithValue("@matname", MatName);
                    cmd.Parameters.AddWithValue("@thickness", 0);
                    cmd.Parameters.AddWithValue("@sheetx", 0);
                    cmd.Parameters.AddWithValue("@sheety", 0);
                    cmd.Parameters.AddWithValue("@yieldrate", 0);
                    cmd.Parameters.AddWithValue("@good", oGood);
                    cmd.Parameters.AddWithValue("@ngqty", oNGQty);
                    cmd.Parameters.AddWithValue("@speed", Speed);
                    cmd.Parameters.AddWithValue("@proctimesecs", ProcTimeSecs);
                    cmd.Parameters.AddWithValue("@alarmtimesecs", AlarmTimeSecs);

                    cmd.Parameters.AddWithValue("@oprtimesecs", OprTimeSecs);

                    cmd.ExecuteNonQuery();
                    counter++;
                }
            }
        }
        public static void extract_eml_processdata(string path, MySqlConnection mcon, MySqlConnection mcon2)
        {
            using (CsvReader csv =
                        new CsvReader(new StreamReader(path), true))
            {
                int counter = 1;
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                while (csv.ReadNextRecord())
                {
                    //for (int i = 0; i < fieldCount; i++)
                    //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));

                    //Date
                    oDate = csv[1];
                    DateTime Date = DateTime.Parse(oDate);

                    //Machine Name
                    MachName = csv[2];
                    string val = "en";
                    if (MachName.StartsWith("ACIES"))
                    {
                        MachName = "ACIES";
                        val = "ac";
                    }
                    else if (MachName.StartsWith("VN3015AJ"))
                    {
                        MachName = "VN3015AJ";
                        val = "vn";
                    }
                    else if (MachName.StartsWith("ENSIS-3015AJ"))
                    {
                        MachName = "ENSIS-3015AJ";
                        val = "en";
                    }

                    //Program Name
                    ProgName = csv[3];

                    //Process start time
                    oProcstarttime = csv[7];
                    DateTime Procstarttime = DateTime.Parse(oProcstarttime);


                    //Process end time
                    oProcendtime = csv[8];
                    DateTime Procendtime = DateTime.Parse(oProcendtime);

                    //Processing Time
                    ProcTime = Procendtime - Procstarttime;
                    double ProcTimeSecs = ProcTime.TotalSeconds;

                    //Machine Operation Time
                    oMachOprTime = csv[9];
                    TimeSpan MachOprTime = TimeSpan.Parse(oMachOprTime);

                    double OprTimeSecs = MachOprTime.TotalSeconds;

                    //Laser processing time
                    oLaserProcTime = csv[10];
                    TimeSpan LaserProcTime = TimeSpan.Parse(oLaserProcTime);

                    //NCT processing time
                    oNCTProcTime = csv[11];
                    if (oNCTProcTime.Contains("-"))
                    {
                        oNCTProcTime = "00:00:00";
                    }
                    NCTProcTime = TimeSpan.Parse(oNCTProcTime);

                    //Alarm Time
                    oAlarmTime = csv[12];
                    if (oAlarmTime.Contains("-"))
                    {
                        oAlarmTime = "00:00:00";
                    }
                    AlarmTime = TimeSpan.Parse(oAlarmTime);
                    double AlarmTimeSecs = AlarmTime.TotalSeconds;

                    //Material name
                    MatName = csv[13];

                    //Thickness
                    oThickness = csv[14];
                    float Thickness = float.Parse(oThickness);

                    //SheetX
                    oSheetX = csv[15];
                    oSheetX = oSheetX.Remove(oSheetX.Length - 3);
                    int SheetX = Int32.Parse(oSheetX);

                    //SheetY
                    oSheetY = csv[16];
                    oSheetY = oSheetY.Remove(oSheetY.Length - 3);
                    int SheetY = Int32.Parse(oSheetY);

                    //Yield rate
                    oYieldRate = csv[17];
                    float YieldRate = float.Parse(oYieldRate);

                    //Good
                    oGood = csv[18];
                    //bool Good = bool.Parse(oGood);
                    //bool Good = Convert.ToBoolean(oGood);

                    //NG Qty
                    oNGQty = csv[19];
                    //bool NGQty = bool.Parse(oNGQty);

                    //Speed
                    Speed = csv[20];

                    /* int def_qty = Convert.ToInt16(oNGQty);
                     if (def_qty != 1 && !String.IsNullOrEmpty(ProgName))
                     {
                             updateEDGEpgminfo(ProgName, oProcstarttime, oProcendtime, "EML3610NT", mcon2);
                     }
                     */
                    string CmdText = "INSERT IGNORE INTO musasicsvextract VALUES(@idcsv,@date,@machname,@progname,@procstrtime,@procendtime,@proctime,@machoprtime,@laserproctime,@nctproctime,@alarmtime,@matname,@thickness,@sheetx,@sheety,@yieldrate,@good,@ngqty,@speed,@proctimesecs,@alarmtimesecs,@oprtimesecs)";
                    MySqlCommand cmd = new MySqlCommand(CmdText, mcon);

                    cmd.Parameters.AddWithValue("@idcsv", oDate + "_" + val + counter);
                    cmd.Parameters.AddWithValue("@date", Date);
                    cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                    cmd.Parameters.AddWithValue("@progname", ProgName);
                    cmd.Parameters.AddWithValue("@procstrtime", Procstarttime);
                    cmd.Parameters.AddWithValue("@procendtime", Procendtime);
                    cmd.Parameters.AddWithValue("@proctime", ProcTime);
                    cmd.Parameters.AddWithValue("@machoprtime", MachOprTime);
                    cmd.Parameters.AddWithValue("@laserproctime", LaserProcTime);
                    cmd.Parameters.AddWithValue("@nctproctime", NCTProcTime);
                    cmd.Parameters.AddWithValue("@alarmtime", AlarmTime);
                    cmd.Parameters.AddWithValue("@matname", MatName);
                    cmd.Parameters.AddWithValue("@thickness", Thickness);
                    cmd.Parameters.AddWithValue("@sheetx", SheetX);
                    cmd.Parameters.AddWithValue("@sheety", SheetY);
                    cmd.Parameters.AddWithValue("@yieldrate", YieldRate);
                    cmd.Parameters.AddWithValue("@good", oGood);
                    cmd.Parameters.AddWithValue("@ngqty", oNGQty);
                    cmd.Parameters.AddWithValue("@speed", Speed);
                    cmd.Parameters.AddWithValue("@proctimesecs", ProcTimeSecs);
                    cmd.Parameters.AddWithValue("@alarmtimesecs", AlarmTimeSecs);

                    cmd.Parameters.AddWithValue("@oprtimesecs", OprTimeSecs);

                    cmd.ExecuteNonQuery();
                    counter++;
                }

            }
        }
        public static void extract_ensis_processdata(string path, MySqlConnection mcon, MySqlConnection mcon2)
        {
            using (CsvReader csv =
                        new CsvReader(new StreamReader(path), true))
            {
                int counter = 1;
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                while (csv.ReadNextRecord())
                {
                    //for (int i = 0; i < fieldCount; i++)
                    //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));

                    //Date
                    oDate = csv[0];
                    DateTime Date = DateTime.Parse(oDate);

                    //Machine Name
                    MachName = csv[1];
                    if (MachName.StartsWith("VN3015AJ"))
                    {
                        MachName = "VN3015AJ";
                    }
                    else if (MachName.StartsWith("ENSIS-3015AJ"))
                    {
                        MachName = "ENSIS-3015AJ";
                    }
                    //Program Name
                    ProgName = csv[2];

                    //Process start time
                    oProcstarttime = csv[3];
                    DateTime Procstarttime = DateTime.Parse(oProcstarttime);


                    //Process end time
                    oProcendtime = csv[4];
                    DateTime Procendtime = DateTime.Parse(oProcendtime);

                    //Processing Time
                    ProcTime = Procendtime - Procstarttime;
                    double ProcTimeSecs = ProcTime.TotalSeconds;

                    //Machine Operation Time
                    oMachOprTime = csv[5];
                    TimeSpan MachOprTime = TimeSpan.Parse(oMachOprTime);

                    double OprTimeSecs = MachOprTime.TotalSeconds;

                    //Laser processing time
                    oLaserProcTime = csv[6];
                    TimeSpan LaserProcTime = TimeSpan.Parse(oLaserProcTime);

                    //NCT processing time
                    oNCTProcTime = csv[7];
                    if (oNCTProcTime.Contains("-"))
                    {
                        oNCTProcTime = "00:00:00";
                    }
                    NCTProcTime = TimeSpan.Parse(oNCTProcTime);

                    //Alarm Time
                    oAlarmTime = csv[8];
                    if (oAlarmTime.Contains("-"))
                    {
                        oAlarmTime = "00:00:00";
                    }
                    AlarmTime = TimeSpan.Parse(oAlarmTime);
                    double AlarmTimeSecs = AlarmTime.TotalSeconds;

                    //Material name
                    MatName = csv[9];

                    //Thickness
                    oThickness = csv[10];
                    float Thickness = float.Parse(oThickness);

                    //SheetX
                    oSheetX = csv[11];
                    oSheetX = oSheetX.Remove(oSheetX.Length - 3);
                    int SheetX = Int32.Parse(oSheetX);

                    //SheetY
                    oSheetY = csv[12];
                    oSheetY = oSheetY.Remove(oSheetY.Length - 3);
                    int SheetY = Int32.Parse(oSheetY);

                    //Yield rate
                    oYieldRate = csv[13];
                    float YieldRate = float.Parse(oYieldRate);

                    //Good
                    oGood = csv[14];
                    //bool Good = bool.Parse(oGood);
                    //bool Good = Convert.ToBoolean(oGood);

                    //NG Qty
                    oNGQty = csv[15];
                    //bool NGQty = bool.Parse(oNGQty);

                    //Speed
                    Speed = csv[16];
                    /* int def_qty = Convert.ToInt16(oNGQty);
                     if (def_qty != 1 && !String.IsNullOrEmpty(ProgName))
                     {

                         if (ProgName.Contains("-") && ProgName.Contains("+"))
                                     {
                             String[] prog_split = ProgName.Split('-');
                             string main_prog = prog_split[0];
                             if (main_prog.Contains("+"))
                             {
                                 String[] prog1_split = main_prog.Split('+');
                                 string main_prog_1 = prog1_split[0];
                                 updateEDGEpgminfo(main_prog_1 + "-" + prog_split[1], oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);
                                 int counter_val = 1;

                                 foreach (string app_val in prog1_split)
                                 {
                                     if (counter_val != 1)
                                     {
                                         int len_newval = app_val.Length;
                                         string new_pg = main_prog_1.Substring(0, (main_prog_1.Length - len_newval)) + app_val;
                                         updateEDGEpgminfo(new_pg + "-" + prog_split[1], oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);

                                     }
                                     counter_val++;
                                 }
                             }
                             else {

                                 String[] append_split = prog_split[1].Split('+');
                                 string tot_pgval = "";
                                 foreach (string app_val in append_split)
                                 {
                                     if (!String.IsNullOrEmpty(tot_pgval))
                                         tot_pgval = tot_pgval + "#" + main_prog + "-" + app_val;
                                     else
                                         tot_pgval = main_prog + "-" + app_val;
                                     updateEDGEpgminfo(main_prog + "-" + app_val, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);
                                 }
                                 updateEDGEpgminfo(tot_pgval, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);
                             }
                         }
                         else if (!ProgName.Contains("-") && ProgName.Contains("+"))
                                       {
                                           String[] prog_split = ProgName.Split('+');
                                           string main_prog = prog_split[0];
                             updateEDGEpgminfo_match(main_prog, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);

                                           int counter_val = 1;

                                           foreach (string app_val in prog_split)
                                           {
                                               if (counter_val != 1)
                                               {
                                                   int len_newval = app_val.Length;
                                                   string new_pg = main_prog.Substring(0, (main_prog.Length - len_newval)) + app_val;
                                             updateEDGEpgminfo_match(new_pg, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);

                                               }
                                               counter_val++;
                                           }

                                       }
                                     else if (ProgName.Contains("-") && ProgName.EndsWith("P"))
                                     {
                                         String[] prog_split = ProgName.Split('-');
                                         string main_prog = prog_split[0];
                                         int count = 1;
                                         foreach (string app_val in prog_split)
                                         {
                                             if (count != 1)
                                             {
                                                 string app_val1 = app_val.Replace("P","");
                                                 updateEDGEpgminfo(main_prog + "-" + app_val1, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);

                                             }
                                             count++;
                                         }

                                     }
                                     else if (!ProgName.Contains("-") && !ProgName.Contains("+"))
                                     {
                                         updateEDGEpgminfo_match(ProgName, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);

                                     }
                                     else
                                         updateEDGEpgminfo(ProgName, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);
 }*/

                    string CmdText = "INSERT IGNORE INTO musasicsvextract VALUES(@idcsv,@date,@machname,@progname,@procstrtime,@procendtime,@proctime,@machoprtime,@laserproctime,@nctproctime,@alarmtime,@matname,@thickness,@sheetx,@sheety,@yieldrate,@good,@ngqty,@speed,@proctimesecs,@alarmtimesecs,@oprtimesecs)";
                    MySqlCommand cmd = new MySqlCommand(CmdText, mcon);

                    cmd.Parameters.AddWithValue("@idcsv", oDate + "_" + "ens" + counter);
                    cmd.Parameters.AddWithValue("@date", Date);
                    cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                    cmd.Parameters.AddWithValue("@progname", ProgName);
                    cmd.Parameters.AddWithValue("@procstrtime", Procstarttime);
                    cmd.Parameters.AddWithValue("@procendtime", Procendtime);
                    cmd.Parameters.AddWithValue("@proctime", ProcTime);
                    cmd.Parameters.AddWithValue("@machoprtime", MachOprTime);
                    cmd.Parameters.AddWithValue("@laserproctime", LaserProcTime);
                    cmd.Parameters.AddWithValue("@nctproctime", NCTProcTime);
                    cmd.Parameters.AddWithValue("@alarmtime", AlarmTime);
                    cmd.Parameters.AddWithValue("@matname", MatName);
                    cmd.Parameters.AddWithValue("@thickness", Thickness);
                    cmd.Parameters.AddWithValue("@sheetx", SheetX);
                    cmd.Parameters.AddWithValue("@sheety", SheetY);
                    cmd.Parameters.AddWithValue("@yieldrate", YieldRate);
                    cmd.Parameters.AddWithValue("@good", oGood);
                    cmd.Parameters.AddWithValue("@ngqty", oNGQty);
                    cmd.Parameters.AddWithValue("@speed", Speed);
                    cmd.Parameters.AddWithValue("@proctimesecs", ProcTimeSecs);
                    cmd.Parameters.AddWithValue("@alarmtimesecs", AlarmTimeSecs);

                    cmd.Parameters.AddWithValue("@oprtimesecs", OprTimeSecs);

                    cmd.ExecuteNonQuery();
                    counter++;
                }

            }
        }
    }
}


